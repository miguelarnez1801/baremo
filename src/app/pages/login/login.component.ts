import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { ToolService} from 'src/app/services/tool.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm:FormGroup;
  constructor(private formBuilder:FormBuilder,
              private auth:AuthService , private tool:ToolService) {
    this.buildForm();
  }

  ngOnInit(): void {
  }

  get username(){
    return this.loginForm.get('email');
  }
  get password(){
    return this.loginForm.get('password');
  }

  buildForm(){
    this.loginForm = this.formBuilder.group({
      email: ['',
      { validators: [Validators.required, Validators.minLength(5), Validators.maxLength(20)]}],
      password: ['', 
      { validators : [Validators.required, Validators.minLength(8), Validators.maxLength(20)]}]
    });
  }
  login(){
    console.log(this.loginForm.value);
    var tok:string;
   this.auth.login(this.loginForm.value).subscribe(
      res=>{
        
       // console.log(res);
      var tokken:string;
      var id:string=res.idUsuario;
      tokken=res.token;
      /* Guardar Datos del login en el Loca Storage*/
      localStorage.setItem('Token', tokken);
      localStorage.setItem('Id', id);
      
      // console.log(tokken);
      /*Obtener Herramientas de los end poins */
      this.tool.getHerramienta(tokken).subscribe(res=>{
        console.log(res);
      },error=>{
      }); 
      /*this.tool.BuscarHerramienta(tokken,2).subscribe(res=>{
          console.log(res);
        },error=>{

        });*/
        /*this.tool.CrearHerramienta(tokken,{"nombre":"Prueba1","descripcion":"Prueba1"}).subscribe(res=>{
          console.log(res);
        },error=>{
        });*/
        /*this.tool.ActualizarHerramienta(tokken,{"nombre":"Prueba2","descripcion":"Prueba2"},'2').subscribe(res=>{
          console.log(res);
        },error=>{
          console.log(error);
        });*/
      },error=>{

      }
    )
  }
 
}
