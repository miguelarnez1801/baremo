import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ToolService {

  constructor( private http : HttpService) { 
  }
  getHerramienta(Token:string):Observable<any>{
    return this.http.getHerramienta('herramienta',Token);
  }
  BuscarHerramienta(Token:string,id:number):Observable<any>{
    return this.http.getBuscarHerramienta('herramienta',Token,id);
  }
  
  CrearHerramienta(Token:string,ToolForm:any):Observable<any>{
    return this.http.postHerramienta('herramienta',Token,ToolForm);
  }
  ActualizarHerramienta(Token:string,ToolForm:any,Id:string):Observable<any>{
    return this.http.putHerramienta('herramienta',Token,ToolForm,Id);
  }
  
}
