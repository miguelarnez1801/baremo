import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class HttpService {

  constructor(
    private httpClient: HttpClient
  ) { }
  
  getHerramienta(serviceName: string,token:any){
    const headers = new HttpHeaders({ 'Authorization': 'Bearer '+token });
    const options = { headers: headers, withCredintials: false };
    const url = environment.apiUrl + serviceName;
    return this.httpClient.get(url, options);
  }
  getBuscarHerramienta(serviceName: string,token:any,Id:number){
    const headers = new HttpHeaders({ 'Authorization': 'Bearer '+token });
    const options = { headers: headers, withCredintials: false };
    const url = environment.apiUrl + serviceName+"/"+Id;
    return this.httpClient.get(url, options);
  }
  
  postHerramienta(serviceName: string,token:any,data:any){
    const headers = new HttpHeaders({ 'Authorization': 'Bearer '+token,'Content-Type': 'application/json'});
    const options = { headers: headers, withCredintials: false };
    const url = environment.apiUrl + serviceName;
    return this.httpClient.post(url,JSON.stringify(data), options);
  }
  putHerramienta(serviceName: string,token:any,data:any,id:string){
    const headers = new HttpHeaders({ 'Authorization': 'Bearer '+token,'Content-Type': 'application/json'});
    const options = { headers: headers, withCredintials: false };
    const url = environment.apiUrl + serviceName+"/"+id;
    return this.httpClient.put(url,JSON.stringify(data), options);
  }
  
  get(serviceName: string) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers: headers, withCredintials: false };
    const url = environment.apiUrl + serviceName;

    return this.httpClient.get(url, options);
  }

  post(serviceName: string, data: any) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers: headers, withCredintials: false };
    const url = environment.apiUrl + serviceName;

    return this.httpClient.post(url, JSON.stringify(data), options);
  }
}