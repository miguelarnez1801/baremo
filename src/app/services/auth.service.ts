import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpService:HttpService) { }

  login(loginForm:any):Observable<any>{
    return this.httpService.post('login', loginForm);
  }
}
